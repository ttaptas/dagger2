package gr.tasos.dagger2.car;

import javax.inject.Inject;

public interface Engine {

    void start();
}
