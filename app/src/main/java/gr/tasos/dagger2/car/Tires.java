package gr.tasos.dagger2.car;

import android.util.Log;

public class Tires {
    private static final String TAG = "Tires";

    public Tires() {
    }

    public void inflate() {
        Log.d(TAG, "Tires inflated...");
    }
}
