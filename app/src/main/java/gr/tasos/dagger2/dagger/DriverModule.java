package gr.tasos.dagger2.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gr.tasos.dagger2.car.Driver;

@Module
public abstract class DriverModule {

    @Provides
    @Singleton
    static Driver provideDriver() {
        return new Driver();
    }
}
