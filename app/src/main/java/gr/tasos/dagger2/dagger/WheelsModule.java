package gr.tasos.dagger2.dagger;

import dagger.Module;
import dagger.Provides;
import gr.tasos.dagger2.car.Rims;
import gr.tasos.dagger2.car.Tires;
import gr.tasos.dagger2.car.Wheels;

@Module
public class WheelsModule {

    @Provides
    static Rims provideRims(){
        return new Rims();
    }

    @Provides
    static Tires provideTires() {
        Tires tires = new Tires();
        tires.inflate();
        return tires;
    }

    @Provides
    static Wheels provideWheels(Rims rims, Tires tires) {
        return new Wheels(rims, tires);
    }
}
