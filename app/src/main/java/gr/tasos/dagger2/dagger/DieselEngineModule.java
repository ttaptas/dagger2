package gr.tasos.dagger2.dagger;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import gr.tasos.dagger2.car.DieselEngine;
import gr.tasos.dagger2.car.Engine;

@Module
public class DieselEngineModule {

    private int horsePower;

    public DieselEngineModule(int horsePower) {
        this.horsePower = horsePower;
    }

    @Provides
    int provideHorsePower() {
        return horsePower;
    }

    @Provides
    Engine provideEngine(DieselEngine engine){
        return engine;
    }
}
