package gr.tasos.dagger2.dagger;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Subcomponent;
import gr.tasos.dagger2.MainActivity;
import gr.tasos.dagger2.car.Car;

@PerActivity
@Subcomponent( modules ={WheelsModule.class, DieselEngineModule.class})
public interface ActivityComponent {

    Car getCar();

    void inject(MainActivity mainActivity);

//    @Component.Builder
//    interface Builder {
//
//        @BindsInstance
//        Builder horsePower(@Named("horsePower") int horsePower);
//
//        @BindsInstance
//        Builder engineCapacity(@Named("engineCapacity") int engineCapacity);
//
//        Builder appComponent(AppComponent appComponent);
//
//        ActivityComponent build();
//    }
}
