package gr.tasos.dagger2.dagger;

import dagger.Binds;
import dagger.Module;
import gr.tasos.dagger2.car.Engine;
import gr.tasos.dagger2.car.PetrolEngine;

@Module
public abstract class PetrolEngineModule {

    @Binds
    abstract Engine bindEngine(PetrolEngine engine);
}
