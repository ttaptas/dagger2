package gr.tasos.dagger2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import javax.inject.Inject;

import gr.tasos.dagger2.car.Car;
import gr.tasos.dagger2.dagger.ActivityComponent;
import gr.tasos.dagger2.dagger.DieselEngineModule;

public class MainActivity extends AppCompatActivity {

    @Inject
    Car car1, car2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityComponent carComponent = ((ExampleApp) getApplication()).getAppComponent()
                .getActivityComponent(new DieselEngineModule(120));

        carComponent.inject(this);

        car1.drive();
        car2.drive();
    }
}
