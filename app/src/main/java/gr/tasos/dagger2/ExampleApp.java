package gr.tasos.dagger2;

import android.app.Application;

import gr.tasos.dagger2.dagger.AppComponent;
import gr.tasos.dagger2.dagger.DaggerAppComponent;

public class ExampleApp extends Application {
    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.create();

    }

    public AppComponent getAppComponent() {
        return component;
    }
}
